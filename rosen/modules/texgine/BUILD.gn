# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/graphic/graphic_2d/graphic_config.gni")

is_ok = true

## Build libtexgine.so {{{
config("libtexgine_config") {
  visibility = [ ":*" ]

  cflags = [
    "-Wall",
    "-Werror",
    "-g3",
    "-std=c++17",
  ]

  include_dirs = [
    "src",
    "export",
  ]
}

config("libtexgine_public_config") {
  include_dirs = [
    "export",
    "texgine_drawing",
    "//third_party/skia/third_party/externals/harfbuzz/src",
    "$graphic_2d_root/rosen/modules/render_service_base/include",
    "$graphic_2d_root/rosen/modules/2d_graphics/src/drawing/engine_adapter",
    "$graphic_2d_root/rosen/modules/2d_graphics/include",
    "$graphic_2d_root/rosen/modules/2d_graphics/src",
    "$graphic_2d_root/rosen/modules/2d_engine/rosen_text/export/rosen_text",
  ]
}

ohos_source_set("libtexgine_source") {
  if (is_ok) {
    sources = [
      "export/symbol_animation/symbol_node_build.cpp",
      "export/symbol_engine/hm_symbol_run.cpp",
      "export/symbol_engine/hm_symbol_txt.cpp",
      "src/bidi_processer.cpp",
      "src/char_groups.cpp",
      "src/dynamic_file_font_provider.cpp",
      "src/dynamic_font_provider.cpp",
      "src/dynamic_font_style_set.cpp",
      "src/font_collection.cpp",
      "src/font_config.cpp",
      "src/font_manager.cpp",
      "src/font_parser.cpp",
      "src/font_providers.cpp",
      "src/font_styles.cpp",
      "src/init.cpp",
      "src/line_breaker.cpp",
      "src/measurer.cpp",
      "src/measurer_impl.cpp",
      "src/mock.cpp",
      "src/opentype_parser/cmap_parser.cpp",
      "src/opentype_parser/cmap_table_parser.cpp",
      "src/opentype_parser/name_table_parser.cpp",
      "src/opentype_parser/opentype_basic_type.cpp",
      "src/opentype_parser/post_table_parser.cpp",
      "src/opentype_parser/ranges.cpp",
      "src/shaper.cpp",
      "src/system_font_provider.cpp",
      "src/texgine_exception.cpp",
      "src/text_breaker.cpp",
      "src/text_converter.cpp",
      "src/text_merger.cpp",
      "src/text_reverser.cpp",
      "src/text_shaper.cpp",
      "src/text_span.cpp",
      "src/text_style.cpp",
      "src/theme_font_provider.cpp",
      "src/typeface.cpp",
      "src/typography_builder_impl.cpp",
      "src/typography_impl.cpp",
      "src/typography_style.cpp",
      "src/typography_types.cpp",
      "src/utils/exlog.cpp",
      "src/utils/logger.cpp",
      "src/utils/memory_reporter.cpp",
      "src/utils/trace_ohos.cpp",
      "src/variant_font_style_set.cpp",
      "src/variant_span.cpp",
      "src/word_breaker.cpp",
    ]

    configs = [
      ":libtexgine_config",
      "//build/config/compiler:exceptions",
    ]

    public_configs = [ ":libtexgine_public_config" ]

    platform = current_os
    if (platform == "mingw") {
      platform = "windows"
    }

    public_deps = [ "texgine_drawing:libtexgine_drawing" ]

    defines = []
    if (defined(use_rosen_drawing) && use_rosen_drawing) {
      defines += [ "USE_ROSEN_DRAWING" ]
      if (ace_enable_gpu) {
        defines += [ "ACE_ENABLE_GPU" ]
      }
    }
    if (enable_text_gine) {
      defines += [ "USE_GRAPHIC_TEXT_GINE" ]
    }
    deps = [ "//third_party/bounds_checking_function:libsec_static" ]
    include_dirs = [ "//third_party/bounds_checking_function/include" ]

    if (platform == "ohos") {
      defines += [ "BUILD_NON_SDK_VER" ]

      if (logger_enable_scope) {
        defines += [ "LOGGER_ENABLE_SCOPE" ]
      }

      if (texgine_enable_debug_log) {
        defines += [ "TEXGINE_ENABLE_DEBUGLOG" ]
      }

      external_deps = [
        "cJSON:cjson_static",
        "c_utils:utils",
        "hilog:libhilog",
        "hitrace:hitrace_meter",
      ]

      public_deps +=
          [ "$graphic_2d_root/rosen/build/icu:rosen_libicu_$platform" ]
    } else {
      sources -= [ "src/utils/trace_ohos.cpp" ]
      if (platform == "mac") {
        defines += [ "BUILD_SDK_MAC" ]
        public_deps += [
          "$graphic_2d_root/rosen/build/harfbuzz:rosen_libharfbuzz_$platform",
        ]
      } else {
        public_deps +=
            [ "$graphic_2d_root/rosen/build/icu:rosen_libicu_$platform" ]
      }
      include_dirs += [ "//third_party/cJSON" ]
      deps += [ "//third_party/cJSON:cjson_static" ]
    }
  }

  part_name = "graphic_2d"
  subsystem_name = "graphic"
}

ohos_shared_library("libtexgine") {
  public_deps = [ ":libtexgine_source" ]

  platform = current_os
  if (platform == "mingw") {
    platform = "windows"
  }

  #the texengine process is only enabled_ Text_ Engine is valid if it is true
  if (enable_text_gine) {
    if (platform == "ohos") {
      deps = [ "$graphic_2d_root/rosen/modules/2d_graphics:2d_graphics" ]
    } else {
      deps = [ "$graphic_2d_root/rosen/modules/2d_graphics:2d_graphics_new" ]
    }
    deps += [ "$graphic_2d_root/rosen/modules/render_service_client:librender_service_client" ]
  }
  innerapi_tags = [ "platformsdk" ]
  part_name = "graphic_2d"
  subsystem_name = "graphic"
}
## Build libtexgine.so }}}
