# Copyright (c) 2023 Huawei Device Co., Ltd.. All rights reserved.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/graphic/graphic_2d/graphic_config.gni")

is_ok = true

config("texgine_drawing_public_config") {
  include_dirs = [
    "./src",
    "../export",
  ]
  cflags = [
    "-Wall",
    "-Werror",
    "-g3",
    "-std=c++17",
  ]
}

if (defined(use_rosen_drawing) && use_rosen_drawing) {
  ohos_source_set("libtexgine_drawing") {
    if (is_ok) {
      sources = [
        "src/texgine_canvas.cpp",
        "src/texgine_dash_path_effect.cpp",
        "src/texgine_data.cpp",
        "src/texgine_font.cpp",
        "src/texgine_font_manager.cpp",
        "src/texgine_font_metrics.cpp",
        "src/texgine_font_style.cpp",
        "src/texgine_font_style_set.cpp",
        "src/texgine_mask_filter.cpp",
        "src/texgine_paint.cpp",
        "src/texgine_path.cpp",
        "src/texgine_path_1d_path_effect.cpp",
        "src/texgine_path_effect.cpp",
        "src/texgine_picture.cpp",
        "src/texgine_rect.cpp",
        "src/texgine_stream.cpp",
        "src/texgine_string.cpp",
        "src/texgine_text_blob.cpp",
        "src/texgine_text_blob_builder.cpp",
        "src/texgine_typeface.cpp",
      ]
    }

    public_configs = [ ":texgine_drawing_public_config" ]
    include_dirs = [
      "$graphic_2d_root/rosen/modules/render_service_base/include",
      "$graphic_2d_root/rosen/modules/2d_graphics/src/drawing/engine_adapter",
      "$graphic_2d_root/rosen/modules/2d_graphics/include",
      "$graphic_2d_root/rosen/modules/2d_graphics/src",
    ]

    platform = current_os
    if (platform == "mingw") {
      platform = "windows"
    }

    public_deps = [ "//third_party/skia:skia_$platform" ]

    if (defined(use_rosen_drawing) && use_rosen_drawing) {
      defines = [ "USE_ROSEN_DRAWING" ]
      if (ace_enable_gpu) {
        defines += [ "ACE_ENABLE_GPU" ]
      }
    }

    part_name = "graphic_2d"
    subsystem_name = "graphic"
  }
} else {
  ohos_shared_library("libtexgine_drawing") {
    if (is_ok) {
      sources = [
        "src/texgine_canvas.cpp",
        "src/texgine_dash_path_effect.cpp",
        "src/texgine_data.cpp",
        "src/texgine_font.cpp",
        "src/texgine_font_manager.cpp",
        "src/texgine_font_metrics.cpp",
        "src/texgine_font_style.cpp",
        "src/texgine_font_style_set.cpp",
        "src/texgine_mask_filter.cpp",
        "src/texgine_paint.cpp",
        "src/texgine_path.cpp",
        "src/texgine_path_1d_path_effect.cpp",
        "src/texgine_path_effect.cpp",
        "src/texgine_picture.cpp",
        "src/texgine_rect.cpp",
        "src/texgine_stream.cpp",
        "src/texgine_string.cpp",
        "src/texgine_text_blob.cpp",
        "src/texgine_text_blob_builder.cpp",
        "src/texgine_typeface.cpp",
      ]
    }

    public_configs = [ ":texgine_drawing_public_config" ]

    platform = current_os
    if (platform == "mingw") {
      platform = "windows"
    }

    public_deps = [ "//third_party/skia:skia_$platform" ]

    deps = []

    if (is_cross_platform) {
      public_deps += [ "foundation/appframework/icu_data:static_icudata" ]
    }

    innerapi_tags = [ "platformsdk" ]
    part_name = "graphic_2d"
    subsystem_name = "graphic"
  }
}
